import Vue from 'vue'
import Router from 'vue-router'
import Create from '@/components/Create'
import Read from '@/components/Read'
import Edit from '@/components/Edit'
import Delete from '@/components/Delete'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'all_post',
      component: Read
    },
    {
      path: '/post-create',
      name: 'create_post',
      component: Create
    },
    {
      name: 'edit_post',
      path: '/post/edit/:id',
      component: Edit
    },
    {
      name: 'delete_post',
      path: '/post/delete/:id',
      component: Delete
    }
  ]
})
